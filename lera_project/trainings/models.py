from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.exceptions import ValidationError
from lera_project.trainers.models import Trainer


class Training(models.Model):
    LOW_SKILLED = 'Low skilled'
    MIDDLE_SKILLED = 'Middle skilled'
    HIGH_SKILLED = 'High skilled'
    CATEGORY_CHOICES = (
        (LOW_SKILLED, _('Low skilled')),
        (MIDDLE_SKILLED, _('Middle skilled')),
        (HIGH_SKILLED, _('High skilled'))
    )
    KIDS = 'Kids'
    ADULTS = 'Adults'
    GROUP_CHOICES = (
        (KIDS, _('Kids')),
        (ADULTS, _('Adults')),
    )
    MONDAY = 'Monday'
    TUESDAY = 'Tuesday'
    WEDNESDAY = 'Wednesday'
    THURSDAY = 'Tuesday'
    FRIDAY = 'Friday'
    SATURDAY = 'Saturday'
    SUNDAY = 'Sandy'
    DAY_CHOICES = (
        (MONDAY, _('Monday')),
        (TUESDAY, _('Tuesday')),
        (WEDNESDAY, _('Wednesday')),
        (THURSDAY, _('Tuesday')),
        (FRIDAY, _('Friday')),
        (SATURDAY, _('Saturday')),
        (SUNDAY, _('Sandy')),
    )
    FIRST = '14-16'
    SECOND = '16-18'
    THIRD = '18-20'
    FORTH = '20-22'
    TIME_CHOICES = (
        (FIRST, '14-16'),
        (SECOND, '16-18'),
        (THIRD, '18-20'),
        (FORTH, '20-22')
    )
    day = models.CharField(max_length=255, choices=DAY_CHOICES)
    time = models.CharField(max_length=6, choices=TIME_CHOICES)
    category = models.CharField(max_length=255, choices=CATEGORY_CHOICES)
    group = models.CharField(max_length=255, choices=GROUP_CHOICES)
    trainer = models.ForeignKey(Trainer)

    def __str__(self):
        return '{}  {}'.format(self.time, self.day)
    # def save(self, *args, **kwargs):
    #     if Training.objects.filter(day=self.day, time=self.time).exists():
    #         return ValidationError("Sprint is incorrect")

