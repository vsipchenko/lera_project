from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.training_index_view, name='index'),
    url(r'^(?P<training_id>[0-9]+)/member/add/$', views.training_member_add_view, name='member_add'),
    url(r'^(?P<training_id>[0-9]+)/member/list/$', views.training_member_list_view, name='member_list'),

]