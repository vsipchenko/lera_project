from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect

from lera_project.trainings.models import Training


def training_index_view(request):
    trainings = Training.objects.all()
    res = {'Monday': ['', '', '', ''], 'Tuesday': ['', '', '', ''], 'Wednesday': ['', '', '', ''],
           'Thursday': ['', '', '', ''], 'Friday': ['', '', '', ''],
           'Saturday': ['', '', '', ''], 'Sunday': ['', '', '', '']}
    for training in trainings:
        if training.time == '14-16':
            res[training.day][0] = training
        if training.time == '16-18':
            res[training.day][1] = training
        if training.time == '18-20':
            res[training.day][2] = training
        if training.time == '20-22':
            res[training.day][3] = training
    return render(request, 'training.html', {'res': res})


def training_member_add_view(request, training_id):
    training = get_object_or_404(Training, pk=training_id)
    training.member.add(request.user)
    return redirect('training:index')


def training_member_list_view(request, training_id):
    training = get_object_or_404(Training, pk=training_id)
    members = User.objects.filter(training=training)
    return render(request, 'training/training_list.html', {'member_list': members})
