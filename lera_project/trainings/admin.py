from django.contrib import admin

from lera_project.trainings.models import Training

admin.site.register(Training)
