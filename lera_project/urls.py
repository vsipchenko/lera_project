from django.conf.urls import url, include
from django.contrib import admin

from general import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home_page, name='home-page'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^login/$', views.login, name='login'),
    url(r'^create_user/$', views.create_user, name='create-user'),
    url(r'^competition/',  include('lera_project.competitions.urls', namespace='competition')),
    url(r'^trainer/',  include('lera_project.trainers.urls', namespace='trainer')),
    url(r'^training/',  include('lera_project.trainings.urls', namespace='training')),

]
