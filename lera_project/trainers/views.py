from django.shortcuts import render
from .models import Trainer


def trainers_index_view(request):
    trainers = Trainer.objects.all()
    return render(request, 'trainer/trainer_list.html', {'trainers': trainers})