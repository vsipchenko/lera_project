from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.trainers_index_view, name='index'),

]
