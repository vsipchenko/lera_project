from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from sorl.thumbnail import get_thumbnail


class Trainer(models.Model):
    FIRST_RANGE = 'First range'
    SECOND_RANGE = 'Second range'
    THIRD_RANGE = 'Third range'
    CANDIDATE = 'Candidate for Master of Sport'
    MASTER = 'Master of Sport '
    MERITED_MASTER = 'Merited Master of Sport'
    INTERNATIONAL_MASTER = 'Master of Sport, International Class'
    CATEGORY_CHOICES = (
        (FIRST_RANGE, _('First range')),
        (SECOND_RANGE, _('Second range')),
        (THIRD_RANGE, _('Third range')),
        (CANDIDATE, _('Candidate for Master of Sport')),
        (MASTER, _('Master of Sport ')),
        (MERITED_MASTER, _('Merited Master of Sport')),
        (INTERNATIONAL_MASTER, _('Master of Sport, International Class')),
    )

    photo = models.ImageField(upload_to='trainer_photo/')
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    sport_category = models.CharField(max_length=255, choices=CATEGORY_CHOICES, null=True, blank=True)
    experience = models.CharField(max_length=255, null=True, blank=True)

    def get_cropped_photo(self, *args, **kwargs):
        return get_thumbnail(self.photo, '136x150', crop='center')

