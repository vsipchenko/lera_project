from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.competition_list_view, name='competition_list'),
    url(r'^(?P<competition_id>[0-9]+)/create-member/$', views.create_competition_member,
        name='create-competition-member'),
    url(r'^(?P<competition_id>[0-9]+)/members/$', views.competition_member_list_view,
        name='member_list'),
]
