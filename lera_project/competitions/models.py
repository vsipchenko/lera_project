from __future__ import unicode_literals
from datetime import date

from django.utils.translation import ugettext_lazy as _
from django.db import models

from lera_project.trainers.models import Trainer


class Competition(models.Model):
    date = models.DateField()
    time = models.TimeField()
    description = models.TextField()
    title = models.CharField(max_length=255, null=True, blank=True)
    place = models.CharField(max_length=255)

    def __str__(self):
        return '({} {}) {}'.format(self.date, self.time, self.title)


class CompetitionMember(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    GENDER_CHOICES = (
        (MALE, _('Male')),
        (FEMALE, _('Female'))
    )
    competition = models.ForeignKey(Competition)
    name = models.CharField(max_length=255)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name=_('Gender'))
    sport_category = models.CharField(max_length=255, choices=Trainer.CATEGORY_CHOICES)
    date_birth = models.DateField(help_text="Format XXXX-XX-XX")
    trainer = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    sport_club = models.CharField(max_length=255, null=True, blank=True)
    phone_first = models.CharField(max_length=14, )

    def calculate_age(self):
        if self.date_birth:
            today = date.today()
            return today.year - self.date_birth.year - (
                (today.month, today.day) < (self.date_birth.month,
                                            self.date_birth.day))
        return False


