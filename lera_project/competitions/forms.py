from django.forms.models import ModelForm

from .models import CompetitionMember
from lera_project.general.forms import CustomFormMixin


class CompetitionMemberForm(CustomFormMixin, ModelForm):
    class Meta:
        model = CompetitionMember
        exclude = ['competition']
