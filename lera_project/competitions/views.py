from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.utils.timezone import datetime
from django.core.urlresolvers import reverse
from django.shortcuts import render

from .forms import CompetitionMemberForm
from .models import Competition, CompetitionMember


def competition_list_view(request):
    competitions = Competition.objects.filter(date__gte=datetime.today().date())
    return render(request, 'competition/competition_list.html', {'competitions': competitions})


def create_competition_member(request, competition_id):
    if Competition.objects.filter(pk=competition_id).exists():
        competition = Competition.objects.get(pk=competition_id)
    else:
        return HttpResponseNotFound('<h1>No competition matching your query</h1>')

    if request.method == 'POST':
        form = CompetitionMemberForm(request.POST)
        if form.is_valid():
            member = form.save(commit=False)
            member.competition = competition
            member.save()
            return HttpResponseRedirect(reverse('competition:competition_list'))
    else:
        form = CompetitionMemberForm()
    return render(request, 'competition/create_member.html', {'form': form, 'competition': competition})


def competition_member_list_view(request, competition_id):
    member_list = CompetitionMember.objects.filter(competition=competition_id)
    return render(request, 'competition/member_list.html', {"member_list": member_list})