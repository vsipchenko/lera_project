from django.contrib import admin

from .models import GeneralInfo


class GeneralInfoAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        if GeneralInfo.objects.exists():
            return False
        return True

admin.site.register(GeneralInfo, GeneralInfoAdmin)
