from django.shortcuts import render
from django.contrib import auth
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .forms import LoginForm, CreateUserForm
from .models import GeneralInfo, Profile


def home_page(request):
    general_info = GeneralInfo.objects.first()
    return render(request, 'home.html', {'info': general_info})


def create_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.pop('password')
            user = form.save(commit=False)
            user.set_password(password)

            user.save()
            return HttpResponseRedirect(reverse('home-page'))
    else:
        form = CreateUserForm()
    return render(request, 'create_user.html', {'form': form})


def login(request):
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            auth.login(request, form._user)
            return HttpResponseRedirect(reverse('home-page'))
    return render(request, 'login.html', {'form': form})


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home-page'))
