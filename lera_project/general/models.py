from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User
from django.db import models

from lera_project.trainings.models import Training


class GeneralInfo(models.Model):
    phone_first = models.CharField(max_length=14, null=True, blank=True)
    phone_second = models.CharField(max_length=14, null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    other = models.CharField(max_length=100, null=True, blank=True)
    club_description = models.TextField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)


class Profile(User):
    MALE = 'M'
    FEMALE = 'F'
    GENDER_CHOICES = (
        (MALE, _('Male')),
        (FEMALE, _('Female'))
    )
    date_birth = models.DateField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    training = models.ManyToManyField(Training, related_name='training', blank=True)
