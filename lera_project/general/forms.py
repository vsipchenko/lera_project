from django.contrib import auth
from django import forms
from django.contrib.auth.models import User

from .models import Profile


class CustomFormMixin(object):
    def __init__(self, *args, **kwargs):
        super(CustomFormMixin, self).__init__(*args, **kwargs)
        for _, field in self.fields.items():
            field.widget.attrs.update( {'class': 'col-xs-10 col-sm-5', 'min-width': '350px'})


class LoginForm(CustomFormMixin, forms.Form):
    username = forms.CharField(label='Username', required=True)
    password = forms.CharField(label='Password', required=True,
                               widget=forms.TextInput(attrs={'type': 'password'}))

    def clean_fields(self):
        username = self.cleaned_data['username']

        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError('User doesn\'t exist.')
        return self.cleaned_data

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = auth.authenticate(username=username, password=password)
            if user and user.is_active:
                self._user = user
            else:
                raise forms.ValidationError('Invalid username or password.')

        return self.cleaned_data


class CreateUserForm(CustomFormMixin, forms.Form):
    username = forms.CharField(label='Username', required=True)
    password = forms.CharField(label='Password', required=True,
                               widget=forms.TextInput(attrs={'type': 'password'}))
    first_name = forms.CharField(label='First name', required=False)
    last_name = forms.CharField(label='Last name', required=False)
    date_birth = forms.DateField(label='Date birth', required=False)
    email = forms.EmailField(label='Email', required=False)
    gender = forms.ChoiceField(label='Gender', choices=Profile.GENDER_CHOICES, required=False)



